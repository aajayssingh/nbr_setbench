#!/bin/sh
##this script runs compiles, runs and then produces nice figures using Setbenches tool framework for DGT and lazylist.  

echo " "
echo "############################################"
echo "Compiling TREE and list..."
echo "############################################"

python3 ../tools/data_framework/run_experiment.py nbr_exp_compile.py -c

# export PATH=$PATH:.
# `chmod +x get_lscpu_numa_nodes.sh`
# `get_lscpu_numa_nodes.sh | awk '{row[NR]=(NR > 1 ? row[NR-1] : 0)+NF} END { row[NR]-=2 ; row[NR+1]=row[1]/2 ; for (i in row) print row[i] }' | sort -n > numa_thread_count.txt`

echo " "
echo " "
echo "############################################"
echo "Executing and generating FIGURES for DGT TREE..."
echo "############################################"
python3 ../tools/data_framework/run_experiment.py nbr_exp_run_tree.py -rdp
# python3 ../tools/data_framework/run_experiment.py nbr_exp.py -dp

# echo "copying FIGURES to plots/expected_plots/ "
# cp data/*.png plots/expected_plots/
echo "copying FIGURES to plots/generated_plots/ "
cp data/*.png plots/generated_plots/

echo " "
echo " "
echo "############################################"
echo "Executing and generating FIGURES for lists..."
echo "############################################"
python3 ../tools/data_framework/run_experiment.py nbr_exp_run_list.py -rdp

# echo "copying FIGURES to plots/expected_plots/ "
# cp data/*.png plots/expected_plots/
echo "copying FIGURES to plots/generated_plots/ "
cp data/*.png plots/generated_plots/
