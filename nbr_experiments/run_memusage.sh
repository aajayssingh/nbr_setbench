#!/bin/sh
##this script runs compiles, runs and then produces nice figures using Setbenches tool framework for DGT for mem usage (thread delay) experiment.  


echo " "
echo "############################################"
echo "Compiling TREE and list..."
echo "############################################"

python3 ../tools/data_framework/run_experiment.py nbr_exp_thread_delay.py -c

echo " "
echo " "
echo "############################################"
echo "Executing and generating FIGURES for DGT TREE..."
echo "############################################"

python3 ../tools/data_framework/run_experiment.py nbr_exp_thread_delay.py -rdp
# python3 ../tools/data_framework/run_experiment.py nbr_exp.py -dp

# echo "copying FIGURES to plots/expected_plots/ "
# cp data/*.png plots/expected_plots/
echo "copying FIGURES to plots/generated_plots/ "
cp data/*.png plots/generated_plots/